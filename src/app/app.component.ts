import { Component } from '@angular/core';

@Component({
    selector: 'tide-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss'],
})
export class AppComponent {
    title = 'TIDE accounts';
}
